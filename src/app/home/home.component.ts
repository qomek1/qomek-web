﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService } from '@app/_services';
import { SignalrService } from '@app/_services/signalr.service';
import { HttpClient } from '@angular/common/http';
@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    users?: User[];
    isVisible = false;
    constructor(private userService: UserService) { }

    ngOnInit() {
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
      }

    showModal(): void {
        this.isVisible = true;
      }
    
      handleOk(): void {
        this.isVisible = false;
        window.location.reload();
      }
    
      handleCancel(): void {
        this.isVisible = false;
        window.location.reload();
      }
}