import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Blog } from '@app/_models/blog';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  constructor(private http: HttpClient) { }

  getAll() {
      return this.http.get<Blog[]>(`${environment.apiUrl}/blog`);
  }

  add(name: string, body: string, createUserId: number){
      return this.http.post(`${environment.apiUrl}/blog`, {name, body, createUserId})
  }
}
