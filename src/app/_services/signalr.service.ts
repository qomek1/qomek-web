import { Injectable } from '@angular/core';
import * as signalR from "@microsoft/signalr"
@Injectable({
  providedIn: 'root'
})
export class SignalrService {
  public allMessages: any[] = [];
  private hubConnection: signalR.HubConnection = new signalR.HubConnectionBuilder() 
  .withUrl('http://localhost:5001/chatHub')
  .build()
    public startConnection = () => {
      this.hubConnection = new signalR.HubConnectionBuilder()
                              .withUrl('http://localhost:5001/chatHub')
                              .build();
      this.hubConnection
        .start()
        .then(() => console.log('Connection started'))
        .catch(err => console.log('Error while starting connection: ' + err))
    }
    
    public addReceiveMessageListener = () => {
      this.hubConnection.on('receiveMessage', (data, username) => {
        console.log(data, username);
        this.allMessages.push({message: data, username: username});
      });
    }
}