import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.less']
})
export class WeatherComponent implements OnInit {

  constructor(private http: HttpClient) { }
  ngOnInit(): void {
  }
  display: any;
  center: google.maps.LatLngLiteral = {
      lat: 24,
      lng: 12
  };
  zoom = 4;
  temp = 0;
  moveMap(event: google.maps.MapMouseEvent) {
      if (event.latLng != null) this.center = (event.latLng.toJSON());
      this.getWeather();
  }
  move(event: google.maps.MapMouseEvent) {
      if (event.latLng != null) this.display = event.latLng.toJSON();
  }

  getWeather(){

    this.http.get<any>("https://api.openweathermap.org/data/2.5/weather?lat=" + this.display?.lat + "&lon=" + this.display?.lng + "&appid=5e20ef2698ff290bb930692635f536c1&units=metric")
    .subscribe((res)=>{
      this.temp = res.main.temp;
    })
  }

}
