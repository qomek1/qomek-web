import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddBlogComponent } from './add-blog/add-blog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BlogFeedComponent } from './blog-feed/blog-feed.component';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzListModule } from 'ng-zorro-antd/list';
import { NzInputModule } from 'ng-zorro-antd/input'
@NgModule({
  declarations: [
    AddBlogComponent,
    BlogFeedComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    NzCardModule,
    NzListModule,
    NzInputModule
  ],
  exports: [
    AddBlogComponent,
    BlogFeedComponent,
  ]
})
export class BlogModule { }
