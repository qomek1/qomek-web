import { Component, OnInit } from '@angular/core';
import { Blog } from '@app/_models/blog';
import { BlogService } from '@app/_services/blog.service';
import { first } from 'rxjs';

@Component({
  selector: 'app-blog-feed',
  templateUrl: './blog-feed.component.html',
  styleUrls: ['./blog-feed.component.less']
})
export class BlogFeedComponent implements OnInit {
  blogs: Blog[] = [];
  constructor(private _blogService: BlogService) { }

  ngOnInit(): void {
    this._blogService.getAll().pipe(first()).subscribe((ret) => {
      this.blogs = ret;
    })
  }

}
