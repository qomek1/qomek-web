import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { BlogService } from '@app/_services/blog.service';

@Component({
  selector: 'app-add-blog',
  templateUrl: './add-blog.component.html',
  styleUrls: ['./add-blog.component.less']
})
export class AddBlogComponent implements OnInit {
    blogForm!: FormGroup;
    loading = false;
    submitted = false;
    error = '';
    @Output()
    onOk = new EventEmitter<string>;

    constructor(
        private formBuilder: FormBuilder,
        public route: ActivatedRoute,
        public router: Router,
        private blogService: BlogService
    ) { 
    }

    ngOnInit() {
        this.blogForm = this.formBuilder.group({
            name: ['', Validators.required],
            body: ['', Validators.required]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.blogForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.blogForm.invalid) {
            return;
        }

        this.error = '';
        this.loading = true;
        const userstring = localStorage.getItem('user');
        const user = JSON.parse(userstring == null ? "" : userstring);
        this.blogService.add(this.f.name.value, this.f.body.value, user.id)
            .pipe(first())
            .subscribe({
                next: () => {
                    this.loading = false;
                    this.onOk.emit("ok");
                },
                error: error => {
                    this.error = error;
                    this.loading = false;
                }
            });
    }

}
