import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SignalrService } from '@app/_services/signalr.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.less']
})
export class ChatComponent implements OnInit {
  message: string = "";
  username: string = "";
  constructor( public signalRService: SignalrService, private http: HttpClient) { }

  ngOnInit(): void {
    this.signalRService.startConnection();
    this.signalRService.addReceiveMessageListener();   
    const userstring = localStorage.getItem('user');
    const user = JSON.parse(userstring == null ? "" : userstring);
    this.username = user.username;
    this.getHistory();
  }
  
  getHistory = () => {
  this.http.post<any[]>('http://localhost:5001/chat', {})
    .subscribe(res => {
      console.log(res);
      res.forEach(data => {
        this.signalRService.allMessages.push({username: data.sender, message: data.message});
      });
    })
  }

  send(){
    this.http.get('http://localhost:5001/chat?message='+ this.message + '&username=' + this.username) 
      .subscribe(res => {
        console.log(res);
      })
  }

}
